#!/usr/bin/env bash

set -uo pipefail

INPUT_DIR="data/input/split"
OUTPUT_DIR="data/output"

retry -timeout 30m -retry-interval 10s sudo protonvpn-cli --disconnect
retry -timeout 30m -retry-interval 10s sudo protonvpn-cli --random-connect

printf "Number of input files: %d\n" $(ls -1 "$INPUT_DIR" | wc -l | cut -d' ' -f1)

for filepath in "$INPUT_DIR"/*; do
    [ -e "$filepath" ] || continue
    file_size=$(wc -l "$filepath" | cut -d' ' -f1)
    printf "Processing file: %s (length: %d)\n" "$filepath" "$file_size"
    
    filename="$(basename $filepath)"
    
    input_filename="$filepath"
    output_filename="$OUTPUT_DIR/$filename.json"
    node run.js "$input_filename" "$output_filename" >> "$OUTPUT_DIR/run_${filename}.log" 2>&1
    
    retry -timeout 30m -retry-interval 10s sudo protonvpn-cli --disconnect
    retry -timeout 30m -retry-interval 10s sudo protonvpn-cli --random-connect
done

printf "Number of results: %d\n" $(find "$OUTPUT_DIR" -type f -name 'x*json' | xargs jq '. | length' | paste -d+ -s | bc)

exit 0
