#!/usr/bin/env bash

set -euo pipefail

INPUT_FILE="company_websites.csv"

n=1
while IFS='~' read company website; do
    company=$(echo "$company" | sed -e "s/'/''/g")
    
    echo "Line No. $n: $company - $website"
    
    query="UPDATE crm_lead SET website = '$website' WHERE name = '$company' AND (website IS NULL OR website = '')"
    
    echo "Executing $query"
    PGPASSWORD=odoo psql -h 'localhost' -U 'odoo' -d 'db' -c "$query"
    
    n=$((n+1))
done < "$INPUT_FILE"
