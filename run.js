'use strict';

const se_scraper = require('se-scraper'),
      process = require('process');

let args = process.argv.slice(2);
if (args.length < 2)
{
    console.err("Please provide an input and an output file")
    process.exit()
}

let input_filename = args[0],
    output_filename = args[1];
    
console.log("Input filename:", input_filename, "\t", "Output filename:", output_filename);

let config = {
    // the user agent to scrape with
    user_agent: '',

    // if random_user_agent is set to True, a random user agent is chosen
    random_user_agent: true,

    // how long to sleep between requests. a random sleep interval within the range [a,b]
    // is drawn before every request. empty string for no sleeping.
    sleep_range: '[10, 20]',

    // which search engine to scrape
    search_engine: 'google',

    // whether debug information should be printed
    // debug info is useful for developers when debugging
    debug: true,

    // whether verbose program output should be printed
    // this output is informational
    verbose: true,

    // an array of keywords to scrape
    keywords: [],

    // alternatively you can specify a keyword_file. this overwrites the keywords array
    keyword_file: input_filename,

    // the number of pages to scrape for each keyword
    num_pages: 1,

    // whether to start the browser in headless mode
    headless: true,

    // specify flags passed to chrome here
    chrome_flags: [], // https://peter.sh/experiments/chromium-command-line-switches/

    // path to output file, data will be stored in JSON
    output_file: output_filename,

    // whether to prevent images, css, fonts from being loaded
    // will speed up scraping a great deal
    block_assets: true,

    // path to js module that extends functionality
    // this module should export the functions:
    // get_browser, handle_metadata, close_browser
    // must be an absolute path to the module
    //custom_func: resolve('examples/pluggable.js'),
    custom_func: '',

    // use a proxy for all connections
    // example: 'socks5://78.94.172.42:1080'
    // example: 'http://118.174.233.10:48400'
    proxy: '',

    // a file with one proxy per line. Example:
    // socks5://78.94.172.42:1080
    // http://118.174.233.10:48400
    proxy_file: '',

    // check if headless chrome escapes common detection techniques
    // this is a quick test and should be used for debugging
    test_evasion: false,
    apply_evasion_techniques: true,

    // log ip address data
    log_ip_address: false,

    // log http headers
    log_http_headers: true,

    puppeteer_cluster_config: {
        timeout: 432000000, // max timeout
        monitor: false,
        concurrency: 1, // one scraper per tab
        maxConcurrency: 1 // scrape with 1 tab
    },
    
    google_settings: {
        google_domain: 'google.ro',
        gl: 'ro', // The gl parameter determines the Google country to use for the query.
        hl: 'ro', // The hl parameter determines the Google UI language to return results.
        start: 0, // Determines the results offset to use, defaults to 0.
        num: 20, // Determines the number of results to show, defaults to 10. Maximum is 100.
    },
};

function callback(err, response) {
    if (err) { console.error(err) }

    /* response object has the following properties:

        response.results - json object with the scraping results
        response.metadata - json object with metadata information
        response.statusCode - status code of the scraping process
     */

    //console.dir(response.results, {depth: null, colors: true});
}

se_scraper.scrape(config, callback);
