#!/usr/bin/env python

import os
import sys
import logging
import json
import csv
import re
from timeit import default_timer as timer
from urllib.parse import urlparse

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class SearchResultsProcessor(object):

    IGNORED_DOMAINS = []

    def __init__(self, input_path):
        self.input_path = input_path
        self.get_ignored_domains()
        self.matches_file = 'company_websites.csv'

    def get_ignored_domains(self):
        ignored_domains_file = "ignored_domains.txt"
        if not os.path.isfile(ignored_domains_file):
            return
        with open(ignored_domains_file, "r") as f:
            SearchResultsProcessor.IGNORED_DOMAINS = SearchResultsProcessor.IGNORED_DOMAINS + f.read().splitlines()

    def run(self):
        if not self.input_path:
            logger.error("Please set input path")
            return

        if os.path.isfile(self.matches_file):
            os.remove(self.matches_file)

        for subdir, dirs, files in os.walk(self.input_path):
            for file in files:
                ext = os.path.splitext(file)[-1].lower()
                if ext == '.json':
                    self.process_file(subdir + os.sep + file)

    def valid_link(self, link):
        if link == 'Website infobox':
            return False

        for ignored_link in SearchResultsProcessor.IGNORED_DOMAINS:
            if ignored_link in link.lower():
                return False

        return True

    def get_infobox_link(self, results):
        for result in results:
            if 'website_infobox' in result and result['website_infobox'] not in [None, '', '#']:
                return result['website_infobox']
        return None

    def get_sanitized_company(self, company):
        for r in ((' SRL-D', ''), (' SRL', ''), (' SA', '')):
            company = re.sub(r[0] + '$', r[1], company)
        for ch in ['"', '&', "'", '(', ')', '+', ',', '-', '.', '@']:
            if ch in company:
                company = company.replace(ch, '')
        company = re.sub(r'\s{2,}', ' ', company)
        return company.strip()

    def get_company_initials(self, company):
        parts = company.split()
        initials = [part[0].lower() for part in parts]
        return ''.join(initials)

    def match_rule_company_initials_in_link(self, company, results):
        initials = self.get_company_initials(company)
        if len(initials) < 3:
            return None

        for result in results:
            link = result['link']
            if self.valid_link(link):
                if initials in link:
                    return link

        return None

    def match_rule_company_name_in_link(self, company, results):
        # TODO: Add logic to prefer .ro websites
        for result in results:
            link = result['link']

            if self.valid_link(link):
                domain = re.sub(r'\.?www\.', '', urlparse(link).netloc)
                dotcount = domain.count('.')

                first_part = None
                if dotcount == 1:
                    first_part = domain[0:domain.find('.')]
                elif dotcount == 2:
                    first_part = domain[domain.find('.'):domain.rfind('.')]

                parts = company.split()

                skip = False
                # Check if all parts are present in link
                for part in parts:
                    if part:
                        if first_part and part.lower() == first_part:
                            break
                        if part.lower() not in domain:
                            skip = True
                            break
                if skip:
                    continue
                return link
        return None

    def match_rule_highest_rank_link(self, results):
        for result in results:
            link = result['link']
            if self.valid_link(link):
                return link
        return None

    def get_matched_link(self, company, results):
        website = None

        if not website:
            website = self.match_rule_company_initials_in_link(company, results)

        if not website:
            website = self.match_rule_company_name_in_link(company, results)

        if not website:
            website = self.match_rule_highest_rank_link(results)

        url = urlparse(website)

        return website[:website.index(url.netloc)] + url.netloc + '/' if website else None

    def get_website(self, company, results):
        website = self.get_infobox_link(results)
        if not website:
            website = self.get_matched_link(company, results)
        return website

    def process_file(self, filepath):
        start = timer()

        if not filepath or not os.path.isfile(filepath):
            logger.error("Empty filepath or it doesn't exist: %s", filepath)
            return False

        logger.info("Processing file: %s", filepath)

        with open(filepath) as json_file:
            with open(self.matches_file, 'a', newline='', encoding='utf-8') as csvfile:
                writer = csv.writer(csvfile, delimiter='~', quotechar='"', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
                companies = json.load(json_file)
                for company_name, company_data in companies.items():
                    if '1' in company_data:
                        results = company_data["1"]["results"]

                        website = self.get_website(self.get_sanitized_company(company_name), results)
                        logger.info("%s - %s", company_name, website)

                        if website:
                            writer.writerow([company_name, website])
                    else:
                        logger.warning(f"Missing data for {company_name}")

        end = timer()
        logger.info("[%s] Elapsed time %f seconds", filepath, end - start)
        return True


if __name__ == '__main__':
    if len(sys.argv) < 2:
        logger.error("Please provide input directory as input")

    input_directory = sys.argv[1]
    logger.info("Input directory: %s", input_directory)

    searchResultsProcessor = SearchResultsProcessor(input_directory)
    searchResultsProcessor.run()
