# pupeteer-search-engine-crawler

Find website for company, given its name, based on search engine results.

## Dependencies
- [protonvpn-cli](https://protonvpn.com/support/linux-vpn-tool/)

- package.json: 
```
-- local
"se-scraper": "file:../se-scraper"

or

-- remote
"se-scraper": "git+https://git@gitlab.com/bcomptec-web/crawlers/se-scraper.git"
```

## Build local se-craper
```
git clone https://gitlab.com/bcomptec-web/crawlers/pupeteer-google-crawler.git ../se-scraper
cd ../se-scraper
npm install
npm run-script build
```

## Split input in chunks
```
mkdir data/input/split && cd data/input/split
split -l 20 ../keywords.csv
```

## Run
Scraper: `./run.sh`  
Results processor: `./process_search_results.py data/output > out.txt 2>&1`  

## Check logs
`tail -F -n 1000 data/output/run_*.log`

## Count output
`find "data/output" -type f -name 'x*json' | xargs jq '. | length' | paste -d+ -s | bc` and 
compare against input: `wc -l "data/input/keywords.csv"`

## Get all links from single result file
`jq -r '.[][]["results"][].link' data/output/xbo.json | grep -P '^(http|www)'`
